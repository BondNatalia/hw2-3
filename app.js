// 1 цикли потрібні для повторення певних операцій багато раз
// 2 for використовують , щоб перебрати певны елементи і виконати з ними якісь дії, while - коли потрібно виконувати певну дію до тих пір, поки не виконається певна умова
// 3 явне перетворення, коли тип змінюємо вручну, неявне - програма сама змінює автоматично

"use strict";
let number = +prompt ("Type any number");
let check = Number.isInteger(number);
while (check===false) {
    number = +prompt ("Type any number");
    check = Number.isInteger(number);
}
if (number < 5) {
    console.log ("Sorry, no numbers");
}

for (let i=5;i<=number;i++) {
  if (i%5 == 0) {
      console.log(i);
  }
}
let m = +prompt ("Type a number");
let n = +prompt ("Type one more number");
let checkM = Number.isInteger(m);
let checkN = Number.isInteger(n);
while (checkM===false || checkN===false) {
    alert("Error. You entered not integer number")
    m = +prompt ("Type a number");
    n = +prompt ("Type one more number");
    checkM = Number.isInteger(m);
    checkN = Number.isInteger(n);
}
let newN;
let newM;
if(m>n) {
    newM = n;
    newN = m;}
    else {
        newM=m;
        newN=n;
    }

nextNumber: for(let i = newM; i <= newN;i++) {
    for (let j=2; j<i; j++)
    {
        if (i%j===0) {continue nextNumber;
        }

    }
    console.log(i);
}